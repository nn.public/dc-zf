## Docker commands

docker ps - show list containers  
docker-compose up - run containers and show command on terminal, for cancel run Cmd+C  
docker-compose up -d - run containers in background mode, for stop see next command  
docker-compose down - stop containers  
docker-compose up [-d] --build - rebuild containers after change Dockerfile or docker-compose.yml  
docker-compose exec [mysql-offboarding | php-offboarding] bash - connect to container (enter on container) for run console command  


## ZF config DB connection

config/autoload/global.php  
```php
    'db' => [
             'driver'         => 'pdo_mysql',
    ]
```

config/autoload/local.php  
```php
    'db' => [
        'username' => 'project',
        'password' => 'qwerty',
        'dbname'   => 'project',
        'host'     => 'mysql-offboarding',
//        'dsn'      => 'mysql:dbname=offboarding;host=localhost',
    ]
```
